var http = require('http');
var url = require('url');
var fs = require('fs');
var uc = require('upper-case');
// var rs = fs.createReadStream('./mynewfile1.txt');
var formidable = require('formidable');

// rs.on('open', function(){
//     console.log('the file is open');
// })

http.createServer(function(req, res){
    var q = url.parse(req.url, true);
    var filename = '.' + q.pathname;

    fs.readFile(filename, function(err, data){
        if(err){
            res.writeHead(404, {'Content-Type': 'text/html; charset=utf-8'});
            return res.end("404 Not Found");
        }

        res.writeHead(200, {'Content-Type': 'text/html; charset=utf-8'});
        res.write(data);
        return res.end();
    });
}).listen(8080);