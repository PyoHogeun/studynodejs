var http = require('http');
var formidable = require('formidable');
var fs = require('fs');
var mysql = require('mysql');

var connection = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: 'pyo1!',
    database: 'pagelist'
});

connection.connect();

connection.query('SELECT * FROM page', function(error, results, fields){
    if (error) throw error;

    console.log(results);
    console.log('The solution is : ' + results[0].solution);
});

connection.end();

http.createServer(function(req, res){
    if(req.url == '/fileupload'){
        var form = new formidable.IncomingForm();
        form.parse(req, function(err, fields, files){
            // console.log(files.filetoupload.name)
            var oldpath = files.filetoupload.path;
            var newpath = 'C:/Users/yjp' + files.filetoupload.name;
            fs.rename(oldpath, newpath, function(err){
                if(err) throw err;
                res.write('File uploaded and move!');
                res.end();
            });
        });
    } else {
        res.writeHead(200, {'Content-Type': 'text/html; charset=utf-8'});
        res.write('<form action="fileupload" method="post" enctype="multipart/form-data">');
        res.write('<input type="file" name="fileupload"><br>');
        res.write('<input type="submit">');
        res.write('</form>');
        return res.end();
    }
}).listen(8080);